package master.ccm2.shareatslocation.messages;

public enum SuccessMessage {
	
	LOCATION_ADD_SUCCESS("Le marqueur a été ajouté avec succès"),
	LOCATION_UPDATE_SUCCESS("Le marqueur a été mis à jour avec succès"),
	LOCATION_DELETE_SUCCESS("Le marqueur a été supprimé avec succès");
	
	private final String successMessage;

	private SuccessMessage(String message) {
		this.successMessage = message;
    }

	public String getSuccessMessage() {
		return successMessage;
	}
	
}
