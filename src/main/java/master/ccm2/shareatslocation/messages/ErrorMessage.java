package master.ccm2.shareatslocation.messages;

public enum ErrorMessage {

	SERVER_ERROR("Erreur du serveur"),
	NOT_FOUND("Marqueur non trouvé");

	private final String errorMessage;

	private ErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
    }

	public String getErrorMessage() {
		return errorMessage;
	}
}