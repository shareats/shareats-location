package master.ccm2.shareatslocation.controllers;

import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import master.ccm2.shareatslocation.interfaces.ManageLocationInterface;
import master.ccm2.shareatslocation.messages.ErrorMessage;
import master.ccm2.shareatslocation.messages.SuccessMessage;
import master.ccm2.shareatslocation.models.Location;

@RestController
public class LocationController {

    @Autowired
    ManageLocationInterface manageLocationService;

    public LocationController() {
    }

    @RequestMapping(value = "/location", method = RequestMethod.PUT)
    public ResponseEntity<String> addLocation(@RequestBody Location newLocation) {
        try {
            manageLocationService.create(newLocation);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return new ResponseEntity<>(ErrorMessage.SERVER_ERROR.getErrorMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(SuccessMessage.LOCATION_ADD_SUCCESS.getSuccessMessage(), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/location/{locationId}", method = RequestMethod.GET)
    public ResponseEntity<Object> getLocation(@PathVariable String locationId) {
        Location location = null;
        try {
            location = manageLocationService.get(locationId);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        if(location == null) {
            return new ResponseEntity<>(ErrorMessage.NOT_FOUND.getErrorMessage(), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(location, HttpStatus.OK);
    }

    @RequestMapping(value = "/location/{locationId}", method = RequestMethod.DELETE)
    public ResponseEntity<String> removeLocation(@PathVariable String locationId) {
        try {
            manageLocationService.delete(locationId);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return new ResponseEntity<>(ErrorMessage.SERVER_ERROR.getErrorMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(SuccessMessage.LOCATION_DELETE_SUCCESS.getSuccessMessage(), HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/locations", method = RequestMethod.GET)
    public ResponseEntity<Object> getLocations(@RequestParam double latitude, @RequestParam double longitude) {
        List<Location> locations = null;
        try {
            locations = manageLocationService.getFromLocation(latitude, longitude);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        if(locations == null) {
            return new ResponseEntity<>(ErrorMessage.NOT_FOUND.getErrorMessage(), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(locations, HttpStatus.OK);
    }

    @RequestMapping(value = "/locations/{authorId}", method = RequestMethod.GET)
    public ResponseEntity<Object> getLocationsByAuthor(@PathVariable String authorId) {
        List<Location> locations = null;
        try {
            locations = manageLocationService.getByAuthor(authorId);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        if(locations == null) {
            return new ResponseEntity<>(ErrorMessage.NOT_FOUND.getErrorMessage(), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(locations, HttpStatus.OK);
    }
}
