package master.ccm2.shareatslocation.interfaces;

import java.util.List;
import java.util.concurrent.ExecutionException;

import master.ccm2.shareatslocation.models.Location;

/**
 * Définition des méthodes pour créer, mettre à jour, supprimer
 */
public interface ManageLocationInterface {

    /**
     * Ajoute un marqueur à la base de données
     * @param newLocation
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public void create(Location newLocation) throws InterruptedException, ExecutionException;

    /**
     * Récupère un marqueur de la base de données
     * @param id Identifiant du marqueur que l'on veut récupérer
     * @return Objet du marqueur à afficher
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public Location get(String id) throws InterruptedException, ExecutionException;
    
    /**
     * Supprime un marqueur de la base de données
     * @param id Identifiant du marqueur
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public void delete(String id) throws InterruptedException, ExecutionException;

    /**
     * Récupère un marqueur dans un rayon d'environ 10km autour des coordonnées passées
     * en paramètre
     * @param latitude Latitude des coordonnées
     * @param longitude Longitude des coordonnées
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public List<Location> getFromLocation(double latitude, double longitude) throws InterruptedException, ExecutionException;

    /**
     * Récupère la liste des marqueurs publiés par l'utilisateur en paramètre
     * @param authorId ID de l'auteur
     * @throws InterruptedException
     * @throws ExecutionException
     */
    public List<Location> getByAuthor(String authorId) throws InterruptedException, ExecutionException;

}