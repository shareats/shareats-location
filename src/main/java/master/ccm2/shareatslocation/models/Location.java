package master.ccm2.shareatslocation.models;

import com.google.cloud.firestore.annotation.DocumentId;

public class Location {

    @DocumentId
    private String documentId;
    private String authorId;
    private String description;
    private CustomGeoPoint location;

    public Location() {
    }

    public Location(String documentId, String authorId, String description, CustomGeoPoint location) {
        this.documentId = documentId;
        this.authorId = authorId;
        this.description = description;
        this.location = location;
    }

    public String getDocumentId() {
        return this.documentId;
    }

    public String getAuthorId() {
        return this.authorId;
    }

    public String getDescription() {
        return this.description;
    }

    public CustomGeoPoint getLocation() {
        return this.location;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLocation(CustomGeoPoint location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "{" +
            " documentId='" + getDocumentId() + "'" +
            ", authorId='" + getAuthorId() + "'" +
            ", description='" + getDescription() + "'" +
            ", location='" + getLocation() + "'" +
            "}";
    }

}