package master.ccm2.shareatslocation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShareatsLocationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShareatsLocationApplication.class, args);
	}

}
