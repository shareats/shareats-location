package master.ccm2.shareatslocation.services;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.GeoPoint;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

import org.springframework.stereotype.Service;

import master.ccm2.shareatslocation.interfaces.ManageLocationInterface;
import master.ccm2.shareatslocation.models.Location;
import master.ccm2.shareatslocation.models.CustomGeoPoint;

@Service
public class FirebaseLocationService implements ManageLocationInterface {

    private final static String LOCATION_COLLECTION = "markers";

    private Firestore firestore;

    public FirebaseLocationService() {
        this.initializeFirebase();
        firestore = FirestoreClient.getFirestore();
    }

    public void create(Location newLocation) throws InterruptedException, ExecutionException {
        this.firestore.collection(LOCATION_COLLECTION).document().set(newLocation);
    }

    public Location get(String id) throws InterruptedException, ExecutionException {
        DocumentReference documentReference = this.firestore.collection(LOCATION_COLLECTION).document(id);
        DocumentSnapshot document = documentReference.get().get();
        Location location = null;

        if(document.exists()) {
            location = buildLocation(document);
        }

        return location;
    }

    public List<Location> getFromLocation(double latitude, double longitude) throws InterruptedException, ExecutionException {
        List<Location> results = new ArrayList<>();
        Map<String, GeoPoint> distanceValues = distanceOperations(latitude, longitude, 6.21372);
        
        ApiFuture<QuerySnapshot> future = this.firestore.collection(LOCATION_COLLECTION)
            .whereGreaterThan("location", distanceValues.get("lesserGeoPoint"))
            .whereLessThan("location", distanceValues.get("greaterGeoPoint"))
            .get();
    
        List<QueryDocumentSnapshot> documents = future.get().getDocuments();
    
        for(DocumentSnapshot document : documents) {
            results.add(buildLocation(document));
        }
    
        return results;
    }

    @Override
    public List<Location> getByAuthor(String authorId) throws InterruptedException, ExecutionException {
        List<Location> results = new ArrayList<>();
        ApiFuture<QuerySnapshot> future = this.firestore.collection(LOCATION_COLLECTION).whereEqualTo("authorId", authorId).get();
        List<QueryDocumentSnapshot> documents = future.get().getDocuments();
    
        for(DocumentSnapshot document : documents) {
            results.add(buildLocation(document));
        }
    
        return results;
    }

    public void update(String id, Location advert) throws InterruptedException, ExecutionException {
        this.firestore.collection(LOCATION_COLLECTION).document(id).set(advert);
    }

    public void delete(String id) throws InterruptedException, ExecutionException {
        this.firestore.collection(LOCATION_COLLECTION).document(id).delete();
    }

    private void initializeFirebase() {

		try {
			FileInputStream serviceAccount = new FileInputStream("firebase-shareats-key.json");
			FirebaseOptions options = new FirebaseOptions.Builder()
					.setCredentials(GoogleCredentials.fromStream(serviceAccount))
					.setDatabaseUrl("https://shareats-e1dc4.firebaseio.com")
                    .build();

            if(FirebaseApp.getApps().isEmpty()) {
                FirebaseApp.initializeApp(options);
            }
        }
        catch (IOException e) {
			e.printStackTrace();
		}
    }

    private Location buildLocation(DocumentSnapshot document) {
        GeoPoint location = document.getGeoPoint("location");
        return new Location(
                document.getId(),
                document.getString("authorId"),
                document.getString("description"),
                new CustomGeoPoint(location.getLatitude(), location.getLongitude())
            );
    }

    private Map<String, GeoPoint> distanceOperations(double latitude, double longitude, double distance) {
        Map<String, GeoPoint> allValues = new HashMap<>();
    
        double lat = 0.0144927536231884;
        double lon = 0.018181818181818;
    
        allValues.put("lesserGeoPoint", new GeoPoint(latitude - (lat * distance), longitude - (lon * distance)));
        allValues.put("greaterGeoPoint", new GeoPoint(latitude + (lat * distance), longitude + (lon * distance)));
    
        return allValues;
    }
}
